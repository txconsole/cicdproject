package com.example.cicdProject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class myController {

	@GetMapping("name/{name}")
	public String getCarrierConfigData(@PathVariable("name") String name) {
		System.out.println("Received CarrierCode: " + name);

		return "Hello " + name;
	}
}
